<?php

$context = Timber::context();

$timber_post     = Timber::get_post();
$context['post'] = $timber_post;
Timber::render(
	array(
		'page-' . $timber_post->post_name . '.twig',
		'page-' . $timber_post->post_type . '.twig',
		'page.twig',
	),
	$context
);
