# WP Composer Base (Twig, Tailwind, Vite, Timber V2)

This guide will help you get our base WordPress stack set up locally, as well as understand the project structure and standard procedures for creating new components, configuring tailwind, and more.

## Prerequisites

Before you begin, make sure you have the following installed on your machine:

- Node.js: [Download and install Node.js](https://nodejs.org)
  - It's recommend to use NVM to switch node versions, this project uses v18. You can use HomeBrew to install NVM.
  - npm: npm is installed with Node.js
- Laravel Valet - Installed through HomeBrew read the valet docs [here](https://laravel.com/docs/11.x/valet#installation)
- Composer - Installed through [HomeBrew](https://formulae.brew.sh/formula/composer#default)

## Getting Started

1. Clone the repository from BitBucket:

```bash
git clone git@bitbucket.org:messageagency/wp-composer-base.git
```

2. Navigate to the project directory:

```bash
cd wp-composer-base
```

Link valet to serve project. Once linked valet will always serve {root-project-folder-name}.test in this case wp-compose-base.test

```bash
valet link
```

3. Install the composer dependencies:

```bash
composer install
```

4. Install the NPM dependencies: Navigate to the sprout theme folder (i.e wp-content/themes/wpsprout)

```bash
npm install
```

4. Start the development server: Your localhost:3000 will be empty, thats normal, please visit the valet proxy. In this case wp-composer-base.test

```bash
npm run dev
```

5. Open your browser and visit [http://localhost:3000](http://localhost:3000) to see the project running.

## Folder Structure

Explain the folder structure of the project and provide a brief description of each folder.

## Configuration

Explain any configuration steps required for the project, such as environment variables or API keys.

## Usage

Provide instructions on how to use the project, including any commands or scripts that are available.

## Javascript

- All JS files will be imported at `src/scripts`
- Use ES6 syntax to export and import components.
- JS components should be created in the folder of the twig component file.

### Example

Explain how others can contribute to the project, such as submitting bug reports or feature requests.

## Tailwind

Specify the license under which the project is distributed.

## Twig Files

Give credit to any resources or individuals that have been helpful in the development of the project.

## NPM Scripts

```bash
"dev": "cross-env NODE_ENV=development vite",
"build": "vite build"
```
