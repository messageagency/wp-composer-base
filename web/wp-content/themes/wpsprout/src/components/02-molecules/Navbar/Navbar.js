export function NavbarDropdownOverride() {
  // Get the dropdown list and button
  const navbarDropdownList = document.querySelectorAll('.fb-dropdown-list')
  const navbarDropdownButton = document.querySelectorAll('.fb-dropdown-button')

  // Overwrite FlowBite styles
  function overwriteFlowBiteDropdownStyles() {
    if (window.innerWidth < 768) {
      navbarDropdownList.forEach((element) => {
        element.style.position = 'static'
        element.style.inset = 'auto'
        element.style.margin = 'auto'
        element.style.transform = 'none'
      })
    } else {
      navbarDropdownList.forEach((element) => {
        element.style.position = 'absolute'
        element.style.inset = '0px auto auto 0px'
        element.style.margin = '0px'
      })
    }
  }

  // Add event listeners
  navbarDropdownButton.forEach((button) => {
    button.addEventListener('click', function () {
      setTimeout(overwriteFlowBiteDropdownStyles, 0)
    })
  })

  // Overwrite FlowBite styles on window resize
  window.addEventListener('resize', overwriteFlowBiteDropdownStyles)
}

export default NavbarDropdownOverride
