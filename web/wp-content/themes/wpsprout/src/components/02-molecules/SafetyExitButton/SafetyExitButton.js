/**
 * This component represents a safety exit feature.
 * It can be used to display a safety exit button or prompt
 * to allow users to exit a certain page or flow safely.
 */

export function SafetyExit() {
  // Get the button
  const safetyExitButton = document.getElementById('safetyExitButton')
  const reDirectURL = 'https://duckduckgo.com/weather?ia=web'

  if (safetyExitButton) {
    // Ensure the button can be activated with the keyboard
    safetyExitButton.addEventListener('keyup', function (event) {
      if (event.key === 'Enter' || event.key === ' ') {
        window.location.href = reDirectURL
      }
    })

    safetyExitButton.addEventListener('click', function () {
      window.location.href = reDirectURL
    })
  }
}

export default SafetyExit
