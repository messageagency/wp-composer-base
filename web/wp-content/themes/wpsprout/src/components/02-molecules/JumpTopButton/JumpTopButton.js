/**
 * This file contains the implementation of the JumpTopButton component.
 * The JumpTopButton component is responsible for displaying a button that allows the user to scroll to the top of the page.
 */

export function JumpTopButton() {
  // Get the button
  const toTopButton = document.getElementById('to-top-button')

  // Check if the button exists
  if (toTopButton) {
    // Show the button when the user scrolls down
    window.addEventListener('scroll', () => {
      if (window.scrollY > 200) {
        // Add the button to the DOM
        toTopButton.classList.remove('hidden')

        // Fade in
        toTopButton.classList.add('opacity-100', 'fixed')
      } else {
        // Fade out
        toTopButton.classList.remove('opacity-100', 'fixed')

        // Remove the button from the DOM
        setTimeout(() => {
          toTopButton.classList.add('hidden') // fade out
        }, 100)
      }
    })

    // Scroll to top
    toTopButton.addEventListener('click', () => {
      window.scrollTo({ top: 0, behavior: 'smooth' })
    })
  }
}

export default JumpTopButton
