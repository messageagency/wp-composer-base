/* --- jQuery
-----------------------------------------------------------------
Not actively used, available if needed.

jQuery is OFF by default.
===============================================================*/
// import $ from "jquery";

/* --- Animate On Scroll (AOS)
-----------------------------------------------------------------
AOS is a small library that allows you to animate elements as you
scroll down, and up.

View the documentation for more information on how to use AOS.
https://michalsnik.github.io/aos/

AOS is OFF by default.
===============================================================*/
// import AOS from 'aos';
// AOS.init();

/* --- FlowBite
-----------------------------------------------------------------
FlowBite is a lightweight and minimal component library that is
a plugin for Tailwind CSS.

https://flowbite.com/
===============================================================*/
import 'flowbite'

/* --- SVG Icons
-----------------------------------------------------------------
FlowBite offers a collection of SVG icons
https://flowbite.com/icons/

You can copy and paste the SVG code from the site above.
FlowBite does not offer a package to import these icons yet
===============================================================*/

/* --- Import Components
-----------------------------------------------------------------
Create all new JS components inside of their respective twig
component folder. See current components for examples.

Components are named using PascalCase.
Components are imported and initialized here.

Use ES6 syntax to export and import components.
See current JS components for examples.
===============================================================*/
import JumpTopButton from '/src/components/02-molecules/JumpTopButton/JumpTopButton.js'
import SafetyExitButton from '/src/components/02-molecules/SafetyExitButton/SafetyExitButton.js'
import Accordion from '/src/components/03-organisms/Accordion/Accordion.js'
import NavbarDropdownOverride from '/src/components/02-molecules/Navbar/Navbar.js'

/* --- Initialize Components
-----------------------------------------------------------------
After importing components, initialize them here. You must do this
for each component you create.
===============================================================*/
JumpTopButton()
SafetyExitButton()
Accordion()
NavbarDropdownOverride()
