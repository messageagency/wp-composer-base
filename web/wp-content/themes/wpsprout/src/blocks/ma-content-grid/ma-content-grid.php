<?php

/**
 * Content Grid Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'content-grid-' . $block['id'];
if (!empty($block['anchor'])) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'content-grid';
if (!empty($block['className'])) {
	$className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
	$className .= ' align' . $block['align'];
}

$context               = Timber::context();
$context['block']      = $block;
$context['field']      = get_fields();
$context['is_preview'] = $is_preview;

$grid_posts = get_field('content_grid_items');
if ($grid_posts) {
    $context['grid_posts'] = array_map(function($grid_post) {
				$post = Timber::get_post($grid_post->ID);
        $post->permalink = get_permalink($grid_post->ID);
        $post->title = get_the_title($grid_post->ID);
        $post->post_type = str_replace(['listing_page', 'generic', 'page'], '', get_post_type($grid_post->ID));
        $post->date = get_the_date('F d, Y', $grid_post->ID);
        $post->image = get_the_post_thumbnail($grid_post->ID, 'tile');

				$image_id = get_post_thumbnail_id($grid_post->ID);
				$image_src = wp_get_attachment_image_src($image_id, 'tile');
				$post->image_src = $image_src ? $image_src[0] : '';
				$post->image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);

        $post->event_date = get_field('event_date', $grid_post->ID);
        $post->resource_type = get_field('resource_type', $grid_post->ID);
        $post->news_type = get_field('news_type', $grid_post->ID);
        $post->location_type = get_field('location_type', $grid_post->ID);
        $post->profile_type = get_field('profile_type', $grid_post->ID);
        return $post;
    }, $grid_posts);
}

$context['overview'] = get_field('band_body');
$context['band_heading'] = get_field('band_heading');
$context['band_body'] = get_field('band_body');
$context['band_ctas'] = get_field('band_ctas');

Timber::render( 'src/components/03-organisms/ContentGrid/ContentGrid.twig', $context );
