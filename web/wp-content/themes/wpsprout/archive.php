<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array( 'archive.twig', 'index.twig' );

$context               = Timber::context();
$context['categories'] = Timber::get_terms( 'post_tag' );
$context['term_page']  = Timber::get_term();

global $paged;
if ( ! isset( $paged ) || ! $paged ) {
	$paged = 1;
}

global $tagparams;
global $params;
$GLOBALS['post'] = $wp_query->post;
setup_postdata( $wp_query->post );

global $tagargs;
$tagargs             = array(
	'post_type'      => 'news',
	'posts_per_page' => 10,
	'tag__in'        => ( get_queried_object() )->term_id,
	'order'          => 'DESC',
	'paged'          => $paged,
);
$context['tagposts'] = Timber::get_posts( $tagargs );

$today = date( 'Ymd' );

$eventargs             = array(
	'post_type'      => 'events',
	'posts_per_page' => 100,
	'meta_query'     => array(
		array(
			'key'     => 'event_date',
			'type'    => 'DATE',
			'compare' => '>=',
			'value'   => $today,
		),
	),
	'meta_key'       => 'event_date',
	'orderby'        => 'meta_value',
	'order'          => 'ASC',
);
$context['eventposts'] = Timber::get_posts( $eventargs );

$newsargs                    = array(
	'post_type' => 'news',
	'p'         => get_field( 'news_feature_id', 'option' ),
	'showposts' => 1,
);
$context['is_featured_news'] = Timber::get_posts( $newsargs );

$context['title'] = 'Archive';
if ( is_day() ) {
	$context['title'] = 'Archive: ' . get_the_date( 'D M Y' );
} elseif ( is_month() ) {
	$context['title'] = 'Archive: ' . get_the_date( 'M Y' );
} elseif ( is_year() ) {
	$context['title'] = 'Archive: ' . get_the_date( 'Y' );
} elseif ( is_tag() ) {
	$context['title'] = single_tag_title( '', false );
	array_unshift( $templates, 'archive-tag.twig' );
} elseif ( is_category() ) {
	$context['title'] = single_cat_title( '', false );
	array_unshift( $templates, 'archive-' . get_query_var( 'cat' ) . '.twig' );
} elseif ( is_post_type_archive() ) {
	$context['title'] = post_type_archive_title( '', false );
	array_unshift( $templates, 'archive-' . get_post_type() . '.twig' );
}


$context['tagposts'] = Timber::get_posts( $tagargs );

Timber::render( $templates, $context );
