<?php
/**
 * Message Agency starter-theme
 * https://github.com/timber/starter-theme
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

 /**
 * If you are installing Timber as a Composer dependency in your theme, you'll need this block
 * to load your dependencies and initialize Timber. If you are using Timber via the WordPress.org
 * plug-in, you can safely delete this block.
 */



 require_once(dirname(ABSPATH, 2) . '/vendor/autoload.php');




  Timber\Timber::init();




/**
 * Sets the directories (inside your theme) to find .twig files
 */
Timber::$dirname = array('src/components/05-templates', 'src/components/05-templates/archives', 'src/components/05-templates/base', 'src/components/05-templates/single', 'src/components/05-templates/search', 'src/components', 'views');

/**
 * By default, Timber does NOT autoescape values. Want to enable Twig's autoescape?
 * No prob! Just set this value to true
 */
Timber::$autoescape = false;

/**
 * We're going to configure our theme inside of a subclass of Timber\Site
 * You can move this to its own file and include here via php's include("MySite.php")
 */
class StarterSite extends Timber\Site
{
	public function __construct()
	{
		add_action('after_setup_theme', array($this, 'theme_supports'));
		add_filter('timber/context', array( $this, 'add_to_context' ) );
		add_filter('timber/twig', array( $this, 'add_to_twig' ) );
		add_action('init', array($this, 'register_post_types'));
		add_action('init', array($this, 'register_taxonomies'));
		add_action('init', array($this, 'register_menus'));
		add_theme_support('post-thumbnails');
		add_theme_support('menus');
		add_theme_support('title-tag');
		parent::__construct();
	}

	public function register_post_types() {

  }
  /** This is where you can register custom taxonomies. */
  public function register_taxonomies() {

  }

	public function add_to_context($context)
	{
		$context['primary_menu']   = Timber::get_menu('primary-menu');
		$context['secondary_menu'] = Timber::get_menu('secondary-menu');
		$context['footer_menu']    = Timber::get_menu('footer-menu');
		$context['site']           = $this;
		return $context;
	}

	public function register_menus()
	{
		register_nav_menu('primary-menu', __('Primary', 'net-centers'));
		register_nav_menu('secondary-menu', __('Secondary', 'net-centers'));
		register_nav_menu('footer-menu', __('Footer', 'net-centers'));
	}
	public function theme_supports()
	{
		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

		/*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
		add_theme_support('post-thumbnails');

		/*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
		add_theme_support(
			'html5',
			array(
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		// Enable support for Post Formats.
		add_theme_support(
			'post-formats',
			array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
				'gallery',
				'audio',
			)
		);

		add_theme_support('menus');

		remove_theme_support('core-block-patterns');
	}

	/** BEM function to pass in bem style classes.
   *
   */
  public function bem($context, $base_class, $modifiers = array(), $blockname = '', $extra = array()) {
    $classes = [];

    // Add the ability to pass an object as the one and only argument.
    if (is_object($base_class) || is_array($base_class)) {
      $object = (object) $base_class;
      unset($base_class);
      $map = [
        'block' => 'base_class',
        'element' => 'blockname',
        'modifiers' => 'modifiers',
        'extra' => 'extra',
      ];
      foreach ($map as $object_key => $arg_key) {
        if (isset($object->$object_key)) {
          $$arg_key = $object->$object_key;
        }
      }
    }

    // Ensure array arguments.
    if (!empty($modifiers) && !is_array($modifiers)) {
      $modifiers = [$modifiers];
    }
    if (!is_array($extra)) {
      $extra = [$extra];
    }

    // If using a blockname to override default class.
    if ($blockname) {
      // Set blockname class.
      $classes[] = $blockname . '__' . $base_class;
      // Set blockname--modifier classes for each modifier.
      if (isset($modifiers) && is_array($modifiers)) {
        foreach ($modifiers as $modifier) {
          $classes[] = $blockname . '__' . $base_class . '--' . $modifier;
        };
      }
    }
    // If not overriding base class.
    else {
      // Set base class.
      $classes[] = $base_class;
      // Set base--modifier class for each modifier.
      if (isset($modifiers) && is_array($modifiers)) {
        foreach ($modifiers as $modifier) {
          $classes[] = $base_class . '--' . $modifier;
        };
      }
    }
    // If extra non-BEM classes are added.
    if (isset($extra) && is_array($extra)) {
      foreach ($extra as $extra_class) {
        $classes[] = $extra_class;
      };
    }
    $attributes = 'class="' . implode(' ', $classes) . '"';
    return $attributes;
  }

  /**
   * Add Attributes function to pass in multiple attributes including bem style classes.
   */
  public function add_attributes( $context, $additional_attributes = [] ) {
    $attributes = [];

    if ( ! empty( $additional_attributes ) ) {
      foreach ( $additional_attributes as $key => $value ) {
        // If there are multiple items in $value as array (e.g., class: ['one', 'two']).
        if ( is_array( $value ) ) {
          $attributes[] = ( $key . '="' . implode( ' ', $value ) . '"' );
        } else {
          // Handle bem() output (pass in exactly the result).
          if ( strpos( $value, '=' ) !== false ) {
            $attributes[] = $value;
          } else {
            $attributes[] = ( $key . '="' . $value . '"' );
          }
        }
      }
    }

    return implode( ' ', $attributes );
  }


  /** This is where you can add your own functions to twig.
   *
   * @param string $twig get extension.
   */
  public function add_to_twig( $twig ) {
    $twig->addExtension( new Twig\Extension\StringLoaderExtension() );
    $twig->addFunction( new \Twig\TwigFunction('bem', array($this, 'bem'), array('needs_context' => true), array('is_safe' => array('html'))) );
    $twig->addFunction( new \Twig\TwigFunction('add_attributes', array($this, 'add_attributes'), array('needs_context' => true), array('is_safe' => array('html'))) );
    return $twig;
  }

}

new StarterSite();

// Namespaces
add_filter('timber/loader/loader', function($loader){
    $loader->addPath(__DIR__ . "/../src/components/01-atoms", "atoms");
    $loader->addPath(__DIR__ . "/../src/components/02-molecules", "molecules");
    $loader->addPath(__DIR__ . "/../src/components/03-organisms", "organisms");
    $loader->addPath(__DIR__ . "/../src/components/04-layouts", "layouts");
		$loader->addPath(__DIR__ . "/../src/components/05-templates", "templates");
		$loader->addPath(__DIR__ . "/../src/components/06-pages", "pages");
    return $loader;
});
