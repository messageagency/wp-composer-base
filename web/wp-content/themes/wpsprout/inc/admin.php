<?php

// Remove post type in generic permalink
function wpsprout_remove_cpt_slug($post_link, $post)
{
	if ('generic' === $post->post_type && 'publish' === $post->post_status) {
		$post_link = str_replace('/' . $post->post_type . '/', '/', $post_link);
	}
	return $post_link;
}
add_filter('post_type_link', 'wpsprout_remove_cpt_slug', 10, 2);
function wpsprout_add_cpt_post_names_to_main_query($query)
{
	// Bail if this is not the main query.
	if (!$query->is_main_query()) {
		return;
	}
	// Bail if this query doesn't match our very specific rewrite rule.
	if (!isset($query->query['page']) || 2 !== count($query->query)) {
		return;
	}
	// Bail if we're not querying based on the post name.
	if (empty($query->query['name'])) {
		return;
	}
	// Add CPT to the list of post types WP will include when it queries based on the post name.
	$query->set('post_type', array('generic', 'news', 'event'));
}
add_action('pre_get_posts', 'wpsprout_add_cpt_post_names_to_main_query');

function add_custom_types_to_tax($query)
{
	if (is_category() || is_tag() && empty($query->query_vars['suppress_filters'])) {
		$post_types = get_post_types();
		$query->set('post_type', $post_types);
		return $query;
	}
}

// Remove default Posts type since no blog
// https://www.mitostudios.com/blog/how-to-remove-posts-blog-post-type-from-wordpress/
// Remove side menu
add_action('admin_menu', 'remove_default_post_type');

function remove_default_post_type()
{
	remove_menu_page('edit.php');
}

// Rename menu items
function rename_pages_menu_name()
{
	global $menu;
	foreach ($menu as $key => $item) {
		if ($item[0] === 'Pages') {
			$menu[$key][0] = __('Landing Page', 'textdomain');
		}
	}
	return false;
}
add_action('admin_menu', 'rename_pages_menu_name', 999);

// Remove comments from theme
// https://wordpress.stackexchange.com/questions/11222/is-there-any-way-to-remove-comments-function-and-section-totally
// Removes from admin menu
add_action('admin_menu', 'my_remove_admin_menus');
function my_remove_admin_menus()
{
	remove_menu_page('edit-comments.php');
	remove_menu_page('edit.php?post_type=profile');
	remove_menu_page('edit.php?post_type=location');
	remove_menu_page('edit.php?post_type=program');
	remove_menu_page('edit.php?post_type=resource');
}
// Removes from post and pages
add_action('init', 'remove_comment_support', 100);

function remove_comment_support()
{
	remove_post_type_support('post', 'comments');
	remove_post_type_support('page', 'comments');
}
// Removes from admin bar
function wpsprout_admin_bar_render()
{
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu('comments');
}
add_action('wp_before_admin_bar_render', 'wpsprout_admin_bar_render');

// Media Functions
// Add svg to allowed media
function wpsprout_mime_types($mimes)
{
	// New allowed mime types.
	$mimes['svg']  = 'image/svg';
	$mimes['svgz'] = 'image/svg+xml';
	$mimes['doc']  = 'application/msword';
	// Optional. Remove a mime type.
	unset($mimes['exe']);
	return $mimes;
}
add_filter('upload_mimes', 'wpsprout_mime_types');


// Query for attachments
// https://stackoverflow.com/questions/51796749/wp-advanced-custom-fields-wysiwyg-insert-link-to-media
add_filter('wp_link_query_args', 'link_query_args');



function link_query_args($query)
{
	$query['post_status'] = array('publish', 'inherit');
	$query['post_type']   = array('news', 'event', 'generic', 'resource', 'page', 'attachment');
	return $query;
}

// Link to media file URL instead of attachment page
add_filter('wp_link_query', 'link_query_results');


function link_query_results($results)
{
	foreach ($results as &$result) {
		if ('Media' === $result['info']) {
			$result['permalink'] = wp_get_attachment_url($result['ID']);
		}
	}
	return $results;
}

// Remove items from admin bar
function wpsprout_remove_from_admin_bar($wp_admin_bar)
{
  $wp_admin_bar->remove_node('themes');
  $wp_admin_bar->remove_node('customize');
  $wp_admin_bar->remove_node('new-post');
  $wp_admin_bar->remove_node('new-listing_page');
  $wp_admin_bar->remove_node('new-event');
  $wp_admin_bar->remove_node('new-program');
  $wp_admin_bar->remove_node('new-location');
  $wp_admin_bar->remove_node('new-resource');
  $wp_admin_bar->remove_node('new-profile');
  $wp_admin_bar->remove_node('wpseo-menu');
}

add_action('admin_bar_menu', 'wpsprout_remove_from_admin_bar', 999);
