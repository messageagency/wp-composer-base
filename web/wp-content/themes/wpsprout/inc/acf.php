<?php

// ACF Functions
// Options Page
if (function_exists("acf_add_options_page")) {
	acf_add_options_page([
		"page_title" => "Theme General Settings",
		"menu_title" => "Theme Settings",
		"menu_slug" => "theme-general-settings",
		"capability" => "edit_posts",
		"redirect" => false,
	]);

	acf_add_options_sub_page([
		"page_title" => "Theme Header Settings",
		"menu_title" => "Header",
		"parent_slug" => "theme-general-settings",
	]);

	acf_add_options_sub_page([
		"page_title" => "Theme Footer Settings",
		"menu_title" => "Footer",
		"parent_slug" => "theme-general-settings",
	]);
}
add_filter("timber/context", "wpsprout_timber_context");

function wpsprout_timber_context($context)
{
	$context["options"] = get_fields("option");
	return $context;
}

// ACF save point folder
// https://www.advancedcustomfields.com/resources/local-json/
add_filter("acf/settings/save_json", "my_acf_json_save_point");
function my_acf_json_save_point($path)
{
	// update path
	$path = get_stylesheet_directory() . "/acf-json";
	// return
	return $path;
}

// Custom ACF MA blocks -- custom functions
add_action("acf/init", "my_acf_init_block_types");
function my_acf_init_block_types()
{
	// MA Billboard block
	if (function_exists("acf_register_block_type")) {
		acf_register_block_type([
			"name" => "billboard",
			"title" => __("Billboard Band"),
			"description" => __("A custom billboard block."),
			"render_template" => "src/blocks/ma-billboard/ma-billboard.php",
			"category" => "design",
			"icon" => "cover-image",
			"keywords" => ["billboard", "ma", "message agency"],
			"mode" => "edit",
			"enqueue_style" =>
			get_template_directory_uri() . "/dist/css/style.css",
		]);
	}

	// MA Story block
	if (function_exists("acf_register_block_type")) {
		acf_register_block_type([
			"name" => "story",
			"title" => __("Story Band"),
			"description" => __("A custom story block."),
			"render_template" => "src/blocks/ma-story/ma-story.php",
			"category" => "design",
			"icon" => "align-pull-left",
			"keywords" => ["story", "ma", "message agency"],
			"mode" => "edit",
		]);
	}

	// MA Signpost block
	if (function_exists("acf_register_block_type")) {
		acf_register_block_type([
			"name" => "signpost",
			"title" => __("Signpost Band"),
			"description" => __("A custom signpost block."),
			"render_template" => "src/blocks/ma-signpost/ma-signpost.php",
			"category" => "design",
			"icon" => "ellipsis",
			"keywords" => ["signpost", "ma", "message agency"],
			"mode" => "edit",
		]);
	}

	// MA Menu Grid block
	if (function_exists("acf_register_block_type")) {
		acf_register_block_type([
			"name" => "menu-grid",
			"title" => __("Menu Grid Band"),
			"description" => __("A menu-grid block."),
			"render_template" => "src/blocks/ma-menu-grid/ma-menu-grid.php",
			"category" => "design",
			"icon" => "screenoptions",
			"keywords" => ["menu", "grid", "ma", "message agency"],
			"mode" => "edit",
		]);
	}

	// MA Accordion block
	if (function_exists("acf_register_block_type")) {
		acf_register_block_type([
			"name" => "accordion",
			"title" => __("Accordion Band"),
			"description" => __("A custom accordion block."),
			"render_template" => "src/blocks/ma-accordion/ma-accordion.php",
			"category" => "design",
			"icon" => "admin-comments",
			"keywords" => ["accordion", "ma", "message agency"],
			"mode" => "edit",
		]);
	}
	// MA Body block
	if (function_exists("acf_register_block_type")) {
		acf_register_block_type([
			"name" => "body",
			"title" => __("Body Band"),
			"description" => __("A custom title and body block."),
			"render_template" => "src/blocks/ma-body/ma-body.php",
			"category" => "design",
			"icon" => "admin-comments",
			"keywords" => [
				"body",
				"text",
				"title",
				"overview",
				"ma",
				"message agency",
			],
			"mode" => "edit",
		]);
	}

	if (function_exists("acf_register_block_type")) {
		acf_register_block_type([
			"name" => "content-grid",
			"title" => __("Content Grid"),
			"description" => __("A content grid block"),
			"render_template" => "src/blocks/ma-content-grid/ma-content-grid.php",
			"category" => "design",
			"icon" => "screenoptions",
			"keywords" => [
				"body",
				"text",
				"title",
				"overview",
				"ma",
				"message agency",
			],
			"mode" => "edit",
		]);
	}

	if (function_exists("acf_register_block_type")) {
		acf_register_block_type([
			"name" => "view-container",
			"title" => __("View Container"),
			"description" => __("A view container block"),
			"render_template" => "src/blocks/ma-view-container/ma-view-container.php",
			"category" => "design",
			"icon" => "screenoptions",
			"keywords" => [
				"body",
				"text",
				"title",
				"overview",
				"ma",
				"message agency",
			],
			"mode" => "edit",
		]);
	}
}

// Allow only custom blocks - Reminder: add all new MA blocks
function wpsprout_allowed_block_types_all($allowed_blocks, $editor_context)
{
	$allowed_blocks = [
		"acf/billboard",
		"acf/story",
		"acf/signpost",
		"acf/menu-grid",
		"acf/body",
		"acf/content-grid",
		"acf/accordion",
		// "acf/view-container",
	];

	$theme_features = get_field("theme_features", "option");


	if ( 'page' === $editor_context->post->post_type ) {
		$allowed_blocks[] = "core/shortcode";

		if (!is_null($theme_features) && !empty($theme_features["accordion"])) {
			$allowed_blocks[] = "acf/accordion";
		}
	}

	return $allowed_blocks;
}

add_filter("allowed_block_types_all", "wpsprout_allowed_block_types_all", 10, 2);
