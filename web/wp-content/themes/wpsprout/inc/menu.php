<?php

// Reorder Admin Menu
function custom_menu_order($menu_ord)
{
	if (!$menu_ord) {
		return true;
	}
	return [
		"index.php", // this represents the dashboard link
		"edit.php?post_type=page", // this is a custom post type menu
		"edit.php?post_type=generic",
		"edit.php?post_type=news",
		"edit.php?post_type=event",
		// "edit.php?post_type=resource",
		// "edit.php?post_type=program",
		// "edit.php?post_type=profile",
		"edit.php?post_type=listing_page",
		"edit.php", // this is the default POST admin menu
		"separator1", // First separator
		"upload.php", // Media
		"wpcf7", // Contact Form 7
		"cfdb7-list.php", // Contact Forms
		"separator2", // Second separator
		"themes.php", // Appearance
		"plugins.php", // Plugins
		"edit.php?post_type=acf-field-group", // ACF
		"theme-general-settings", // Theme settings
		"users.php", // Users
		"options-general.php", // Settings
		"tools.php", // Tools
		"separator3", // Third separator
		"ivory-search", // Ivory Search
	];
}
add_filter("custom_menu_order", "custom_menu_order", 10, 1);
add_filter("menu_order", "custom_menu_order", 10, 1);
