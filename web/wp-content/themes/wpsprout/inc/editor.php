<?php

// Disable Core Functions
// Disable Gutenberg on generic pages
// https://www.billerickson.net/disabling-gutenberg-certain-templates/
function ea_disable_editor($id = false)
{
	$excluded_templates = ["Generic Page"];
	$excluded_ids = [];
	if (empty($id)) {
		return false;
	}
	$id = intval($id);
	$template = get_page_template_slug($id);
	return in_array($id, $excluded_ids) ||
		in_array($template, $excluded_templates);
}

// Disable Gutenberg by template
// https://www.billerickson.net/disabling-gutenberg-certain-templates/
function ea_disable_gutenberg($can_edit, $post_type)
{
	if (!(is_admin() && !empty($_GET["post"]))) {
		return $can_edit;
	}

	if (ea_disable_editor($_GET["post"])) {
		$can_edit = false;
	}

	return $can_edit;
}
add_filter("use_block_editor_for_post_type", "ea_disable_gutenberg", 10, 2);

// Disable Classic Editor by template
// https://www.billerickson.net/code/hide-editor-on-specific-page-template/
function ea_disable_classic_editor()
{
	$screen = get_current_screen();
	if ("page" !== $screen->id || !isset($_GET["post"])) {
		return;
	}

	if (ea_disable_editor($_GET["post"])) {
		remove_post_type_support("page", "editor");
	}
}
add_action("admin_head", "ea_disable_classic_editor");

// Add Formats dropdown to wysiwyg
// Adding button styles, but can add other formatting options
// https://torquemag.io/2015/08/customize-wordpress-wysiwyg-editor/
function add_style_select_buttons($buttons)
{
	array_unshift($buttons, 'styleselect');
	return $buttons;
}
// Register our callback to the appropriate filter
add_filter('mce_buttons_2', 'add_style_select_buttons');

// Add Formats dropdown to wysiwyg
function wpsprout_custom_styles($init_array)
{

	$style_formats = array(
		// These are the custom styles
		array(
			'title'   => 'Primary Button',
			'block'   => 'a',
			'href'    => '#',
			'classes' => 'btn btn-primary',
			'wrapper' => true,
		),
		array(
			'title'   => 'Secondary Button',
			'block'   => 'a',
			'href'    => '#',
			'classes' => 'btn btn-secondary',
			'wrapper' => true,
		),
	);
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode($style_formats);
	return $init_array;
}
// Attach callback to 'tiny_mce_before_init'
add_filter('tiny_mce_before_init', 'wpsprout_custom_styles');

// Add Formats dropdown to wysiwyg
function wpsprout_block_formats($args)
{
	$args['block_formats'] = 'Heading=h2;Subheading=h3;Paragraph=p;';
	return $args;
}
add_filter('tiny_mce_before_init', 'wpsprout_block_formats');


// Move the SEO metabox to the bottom of the page
add_filter('wpseo_metabox_prio', function () {
	return 'low';
});

add_filter('rest_prepare_taxonomy', function ($response, $taxonomy, $request) {
	$context = !empty($request['context']) ? $request['context'] : 'view';
	// Context is edit in the editor
	if ($context === 'edit' && $taxonomy->meta_box_cb === false) {
		$data_response = $response->get_data();
		$data_response['visibility']['show_ui'] = false;
		$response->set_data($data_response);
	}
	return $response;
}, 10, 3);
