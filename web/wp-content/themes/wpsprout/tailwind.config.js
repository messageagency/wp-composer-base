const plugin = require('tailwindcss/plugin')

module.exports = {
  content: [
    // https://tailwindcss.com/docs/content-configuration
    // "./*.php",
    // "./inc/**/*.php",
    //'./**/*.php', // recursive search for *.php (be aware on every file change it will go even through /node_modules which can be slow, read doc)
    // "./**/*.twig",
    './templates/**/*.twig',
    './src/**/*.twig',
    './node_modules/flowbite/**/*.js',
  ],
  safelist: ['text-black', 'text-white'],
  corePlugins: {
    preflight: true,
  },
  variants: {
    extend: {
      brightness: ['group-hover'],
      opacity: ['group-hover'],
    },
  },
  blockList: [],
  theme: {
    screens: {
      sm: '640px',
      // Generates => @media (min-width: 640px)
      md: '768px',
      // Generates => @media (min-width: 768px)
      lg: '1024px',
      // Generates => @media (min-width: 1024px)
      xl: '1280px',
      // Generates => @media (min-width: 1280px)
      '2xl': '1536px',
      // Generates => @media (min-width: 1536px)
    },
    container: {
      center: true,
      screens: {
        sm: '100%',
        md: '100%',
        lg: '1128px',
        xl: '1128px',
      },
    },
    fontFamily: {
      sans: [
        'Montserrat',
        'sans-serif',
        '-apple-system',
        'BlinkMacSystemFont',
        'Segoe UI',
        'Roboto',
        'Helvetica Neue',
        'Arial',
        'Noto Sans',
        'Apple Color Emoji',
        'Segoe UI Emoji',
        'Segoe UI Symbol',
        'Noto Color Emoji',
      ],
      serif: ['Merriweather', 'serif'],
      archivo: ['Archivo', 'sans-serif'],
      mulish: ['Mulish', 'sans-serif'],
    },
    fontSize: {
      none: '0', // 12px
      xs: '0.75rem', // 12px
      sm: '0.875rem', // 14px
      base: '1rem', // 16px
      lg: '1.125rem', // 18px
      xl: '1.25rem', // 20px
      '2xl': '1.5rem', // 24px
      '3xl': '1.875rem', // 30px
      '4xl': '2.25rem', // 36px
      '5xl': '3rem', // 48px
      '6xl': '4rem', // 64px
      '7xl': '5rem', // 80px
      '8xl': '6rem', // 96px
      '9xl': '8rem', // 128px
    },
    fontWeight: {
      hairline: 100,
      thin: 200,
      light: 300,
      normal: 400,
      medium: 500,
      semibold: 600,
      bold: 700,
      extrabold: 800,
      black: 900,
    },
    spacing: {
      0: '0',
      1: '8px',
      2: '16px',
      3: '24px',
      4: '32px',
      5: '40px',
      6: '48px',
      7: '56px',
      8: '64px',
      9: '72px',
      10: '80px',
      11: '88px',
      12: '96px',
      13: '104px',
      14: '112px',
      15: '120px',
      16: '128px',
      17: '136px',
      18: '144px',
      19: '152px',
      20: '160px',
    },

    extend: {
      colors: {
        'brand-primary': '#75BEE9',
        'brand-secondary': '#358DD4',
        'brand-dark': '#02388D',
        transparent: 'transparent',
        white: '#ffffff',
        'off-white': '#F0F1F4',
        black: '#000000',
        'brand-blue': '#043c76',
        'brand-light-blue': '#DBEFFF',
        'brand-indigo': '#6610f2',
        'brand-purple': '#6f42c1',
        'brand-pink': '#d63384',
        'brand-red': '#dc3545',
        'brand-orange': '#fd7e14',
        'brand-yellow': '#ffc107',
        'brand-green': '#b1bd2e',
        'brand-teal': '#80cac8',
        'brand-cyan': '#0dcaf0',
        'brand-gray': '#5C5D5D',
        'brand-gray-light': '#E5E5E5',
      },
      fontSize: {},
      textColor: {
        'body-color': '#000',
        'link-color': '#3490dc',
        'heading-color': '#000',
      },
      fontFamily: {
        fa: ['Font Awesome 5 Free'],
      },
      lineHeight: {
        11: '44px',
        12: '48px',
        13: '52px',
        14: '56px',
        15: '60px',
        16: '64px',
        17: '68px',
        18: '72px',
        19: '76px',
        20: '80px',
      },
      aspectRatio: {
        'card-image': '3 / 2',
      },
      transitionDuration: {
        250: '250ms',
      },
    },
  },
  plugins: [
    require('flowbite/plugin'),
    require('@tailwindcss/forms')({
      strategy: 'base',
    }),
    plugin(function ({ addComponents, theme }) {
      addComponents({
        //----------------
        // BTN - BASE
        //----------------
        '.ma--btn': {
          color: theme('text.black'),
          fontWeight: '700',
          fontSize: '16px',
          textTransform: 'uppercase',
          lineHeight: '24px',
          padding: '16px 24px',
          minWidth: '126px',
          maxWidth: 'max-content',
          minHeight: '48px',
          display: 'inline-block',
          textAlign: 'center',
          letterSpacing: '1.23px',
          display: 'inline-flex',
          alignItems: 'center',
          justifyContent: 'center',
          textWrap: 'wrap',
          // borderRadius: '4px',
          '@apply transition-colors duration-250 ease-in-out': {
            transitionProperty: 'color, background-color',
            transitionDuration: '250ms',
            transitionTimingFunction: 'cubic-bezier(0.4, 0, 0.2, 1)',
            transitionDelay: '0s',
          },

          '&:hover': { textDecoration: 'none' },
          '&:active': { textDecoration: 'none' },
          '&:focus': { textDecoration: 'none' },
          '@screen sm': {},
          '@screen md': {},
        },

        //----------------
        // BTN - PRIMARY
        //----------------
        '.ma--btn--primary': {
          color: theme('colors.black'),
          backgroundColor: theme('colors.brand-primary'),

          '&:hover': {
            backgroundColor: theme('colors.brand-secondary'),
          },

          '&:active': {
            backgroundColor: theme('colors.brand-primary'),
          },

          '&:focus': {
            backgroundColor: theme('colors.brand-primary'),
            borderWidth: '1px',
            borderColor: theme('colors.white'),
            borderStyle: 'solid',
            boxShadow: '0 0 0 1px #000',
          },

          '&:disabled': {
            color: theme('colors.brand-gray'),
            borderWidth: '1px',
            borderColor: theme('colors.brand-gray-light'),
            borderStyle: 'solid',
            backgroundColor: theme('colors.off-white'),
            cursor: 'not-allowed',
            boxShadow: '0 0 0 0px transparent',
          },
        },

        //----------------
        // BTN - SECONDARY
        //----------------
        '.ma--btn--secondary': {
          backgroundColor: theme('colors.white'),
          color: theme('colors.black'),
          borderWidth: '2px',
          borderColor: theme('colors.black'),
          borderStyle: 'solid',

          '&:hover': {
            color: theme('colors.white'),
            backgroundColor: theme('colors.black'),
          },

          '&:active': {
            backgroundColor: theme('colors.white'),
          },

          '&:focus': {
            color: theme('colors.white'),
            backgroundColor: theme('colors.black'),
            boxShadow: '0 0 0 1px #000',
            borderWidth: '1px',
            borderColor: theme('colors.white'),
            borderStyle: 'solid',
          },

          '&:disabled': {
            color: theme('colors.brand-gray'),
            borderWidth: '1px',
            borderColor: theme('colors.brand-gray-light'),
            borderStyle: 'solid',
            backgroundColor: theme('colors.off-white'),
            cursor: 'not-allowed',
            boxShadow: '0 0 0 0px transparent',
          },
        },

        //----------------
        // BLOCK SPACING
        //----------------
        '.ma--block--spacing': {
          padding: '40px 16px',
          '@screen sm': {},
          '@screen md': {
            padding: '80px 16px',
          },
          '@screen lg': {},
        },

        //----------------
        // BILLBOARD
        //----------------
        '.ma--billboard': {
          backgroundColor: theme('colors.brand-light-blue'),
          width: '100%',
          maxHeight: 'max-content',
          minHeight: '600px',
          textAlign: 'center',
          p: {
            color: 'inherit',
            padding: '0',
            '@screen sm': {
              lineHeight: '32px',
            },
          },
        },

        //----------------
        // STORY
        //----------------
        '.ma--story': {
          backgroundColor: theme('colors.brand-light-blue'),
          minHeight: '600px',

          h2: {
            fontWeight: '700',
            lineHeight: '32px',
            letterSpacing: 'normal',
            padding: '0 0 16px 0',
            '@screen sm': {},
            '@screen md': {
              lineHeight: '46px',
              padding: '0 0 24px 0',
            },
          },

          p: {
            color: theme('colors.black'),
            lineHeight: '28px',
            fontSize: '16px',
            '@screen sm': {},
            '@screen md': {
              fontSize: '20px',
              lineHeight: '30px',
            },
          },
          iframe: {
            // maxHeight: '90%',
            margin: '24px 0',
            width: '100%',
            '@screen md': {
              height: '462px',
            },
          },
        },

        //----------------
        // SIGNPOST
        //----------------
        '.ma--signpost': {
          h2: {
            fontSize: '30px',
            lineHeight: '38px',

            '@screen md': {
              fontSize: '56px',
              lineHeight: '68px',
            },
          },
          h3: {
            fontSize: '20px',
            lineHeight: '30px',
            '@screen md': {
              fontSize: '27px',
              lineHeight: '36px',
            },
          },
          p: {
            fontSize: '16px',
            lineHeight: '28px',
            '@screen md': {
              fontSize: '18px',
              lineHeight: '27px',
            },
          },
          i: {
            fontSize: '50px',
            color: theme('colors.brand-primary'),
            '@screen md': {
              fontSize: '80px',
            },
          },
        },

        //----------------
        // CONTENT GRID
        //----------------
        '.ma--content--grid': {
          padding: '40px 16px 80px 16px',
        },

        //----------------
        // GRID CARD
        //----------------
        '.ma--grid--card': {
          backgroundColor: theme('colors.brand-primary'),
          minHeight: '200px',
          minWidth: '360px',
          backgroundSize: 'cover',
          padding: '24px',
          position: 'relative',
          zIndex: '0',

          '&::before': {
            content: '""',
            display: 'block',
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0,0,0,0)',
            position: 'absolute',
            left: '0',
            top: '0',
            zIndex: '1',
            transition: 'background-color 150ms ease-in-out',
          },
          '&:hover': {
            '&::before': {
              backgroundColor: 'rgba(0,0,0,0.6)',
            },
          },
        },

        //----------------
        // WIDE CARD
        //----------------
        '.ma--wide--card': {
          h2: {
            fontSize: theme('fontSize.2xl'),
            fontWeight: theme('fontWeight.medium'),
          },
        },

        //----------------
        // NAVIGATION
        //----------------
        '.ma--nav': {
          height: '64px',
        },

        //----------------
        // MENU GRID
        //----------------
        '.ma--menugrid': {},

        //----------------
        // ACCORDION
        //----------------
        '.ma--accordion': {},
      })
    }),
  ],
}
