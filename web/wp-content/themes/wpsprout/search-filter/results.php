<?php
/**
 * Search & Filter Pro
 *
 * Sample Results Template
 *
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 *
 * Note: these templates are not full page templates, rather
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think
 * of it as a template part
 *
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs
 * and using template tags -
 *
 * http://codex.wordpress.org/Template_Tags
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( $query->have_posts() ) {

	$result_posts = [];
	while ( $query->have_posts() ) {
		$query->the_post();

			$permalink = get_permalink();
			$title = get_the_title();
			$body =  wp_trim_words(get_post_meta( get_the_ID(), 'body', true ), 40);
			$thumbnail_url = has_post_thumbnail() ? get_the_post_thumbnail_url(get_the_ID(), 'small') : '';
			$alt_text = get_post_meta(get_the_ID(), '_wp_attachment_image_alt', true);
			$categories = get_the_category() ? implode(', ', wp_list_pluck($categories, 'name')) : '';
			$tags = get_the_tags() ? implode(', ', wp_list_pluck($tags, 'name')) : '';
			$date = get_the_date();
	}

	Timber::render('/src/components/03-organisms/Card/WideCard.twig',
		[
			'wide_card_title' => $title,
      'wide_card_body' => $body,
      'wide_card_permalink' => $permalink,
      'wide_card_image' => $thumbnail_url,
      'wide_card_image_alt' => $alt_text,
      'wide_card_date' => $date,
			'wide_card_categories' => $categories,
			'wide_card_tags' => $tags
		]
	);
	?>

	<div class="pagination">
		<?php
			/* example code for using the wp_pagenavi plugin */
			if ( function_exists( 'wp_pagenavi' ) ) {
				wp_pagenavi( array( 'query' => $query ) );

			}
		?>
	</div>
	<?php
} else {
	echo 'No Results Found';
}
?>
