<?php
include 'inc/setup.php';
include 'inc/acf.php';
include 'inc/admin.php';
include 'inc/editor.php';
include 'inc/images.php';
include 'inc/menu.php';
include "vite_press.php";


// Add custom image sizes to theme
add_image_size('banner', 1920, 1080, true);
add_image_size('billboard', 1920, 1080, true);
add_image_size('full', 1200, 800, false);
add_image_size('story', 1110, 912, true);
add_image_size('teaser', 528, 410, true);
add_image_size('tile', 720, 480, true);



// Move the SEO metabox to the bottom of the page
add_filter('wpseo_metabox_prio', function () {
	return 'low';
});


add_filter('rest_prepare_taxonomy', function ($response, $taxonomy, $request) {
	$context = !empty($request['context']) ? $request['context'] : 'view';
	// Context is edit in the editor
	if ($context === 'edit' && $taxonomy->meta_box_cb === false) {
		$data_response = $response->get_data();
		$data_response['visibility']['show_ui'] = false;
		$response->set_data($data_response);
	}
	return $response;
}, 10, 3);


// Disable global styles from WordPress
function disable_global_styles() {
    remove_theme_support('wp-block-styles');
    remove_theme_support('editor-styles');
    remove_theme_support('dark-editor-style');
    remove_theme_support('responsive-embeds');
}

add_action('after_setup_theme', 'disable_global_styles');
