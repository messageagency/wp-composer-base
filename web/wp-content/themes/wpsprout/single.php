<?php

use Timber\Timber;

$context         = Timber::context();
$context['post'] = Timber::get_post();

$news_topics = get_the_terms( get_the_ID(), 'topic' );
$news_topic_list = wp_list_pluck( $news_topics, 'slug' );

$newsargs = [
	'post_type'      => 'news',
	'posts_per_page' => 3,
	'post__not_in'   => [get_the_ID()],
];

$context['related_news'] = Timber::get_posts( $newsargs );

$resource_topics = get_the_terms( get_the_ID(), 'topic' );
$resource_topic_list = wp_list_pluck( $resource_topics, 'slug' );

$resourceargs = [
	'post_type'      => 'resource',
	'posts_per_page' => 3,
	'post__not_in'   => [get_the_ID()],
	'orderby' => 'rand',
	'tax_query' => array(
		array(
			'taxonomy' => 'topic',
			'field' => 'slug',
			'terms' => $resource_topic_list
		)
	)
];

$context['related_resources'] = Timber::get_posts( $resourceargs );

$program_topics = get_the_terms( get_the_ID(), 'topic' );
$program_topic_list = wp_list_pluck( $resource_topics, 'slug' );

$programargs = [
	'post_type'      => 'program',
	'posts_per_page' => 3,
	'post__not_in'   => [get_the_ID()],
	'orderby' => 'rand',
	'tax_query' => array(
		array(
			'taxonomy' => 'topic',
			'field' => 'slug',
			'terms' => $program_topic_list
		)
	)
];

$context['related_programs'] = Timber::get_posts( $programargs );

$location_topics = get_the_terms( get_the_ID(), 'topic' );
$location_topic_list = wp_list_pluck( $resource_topics, 'slug' );

$locationargs = [
	'post_type'      => 'location',
	'posts_per_page' => 3,
	'post__not_in'   => [get_the_ID()],
	'orderby' => 'rand',
	'tax_query' => array(
		array(
			'taxonomy' => 'topic',
			'field' => 'slug',
			'terms' => $location_topic_list
		)
	)
];

$context['related_locations'] = Timber::get_posts( $locationargs );

$profile_topics = get_the_terms( get_the_ID(), 'profile-type' );
$profile_topic_list = wp_list_pluck( $profile_topics, 'slug' );

$profileargs = [
	'post_type'      => 'profile',
	'posts_per_page' => 3,
	'post__not_in'   => [get_the_ID()],
	'orderby' => 'rand',
	'tax_query' => array(
		array(
			'taxonomy' => 'profile-type',
			'field' => 'slug',
			'terms' => $profile_topic_list
		)
	)
];

$context['related_profiles'] = Timber::get_posts( $profileargs );

$today = date( 'Ymd' );

$eventargs              = [
	'post_type'      => 'event',
	'posts_per_page' => 3,
	'post__not_in'   => [get_the_ID()],
	'meta_query'     => array(
		array(
			'key'     => 'date_start',
			'type'    => 'DATE',
			'compare' => '>=',
			'value'   => $today,
		),
	),
	'meta_key'       => 'date_start',
	'orderby'        => 'meta_value',
	'order'          => 'ASC',
];

$context['related_events'] = Timber::get_posts( $eventargs );

Timber::render(
	[
		'single-' . $post->post_type . '.twig',
		'single.twig',
	],
	$context
);
