/* --- Get Styles 
-----------------------------------------------------------------
This is the main entry point for for all styles. This is needed
by Vite to compile the styles.
===============================================================*/
import './src/styles.css'

/* --- Get Scripts
-----------------------------------------------------------------
This is the main entry point for all scripts. This is needed by
Vite to compile the scripts.
===============================================================*/
import './src/scripts.js'
